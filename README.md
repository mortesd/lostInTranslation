Project by: Morten Svedjan Dahl and Sondre Relling

This app translates letters into sign language, and stores the 10 last searches to your user.

Heroku: https://sign-language-translator-awzm.herokuapp.com/

