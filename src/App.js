import './App.css';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";

import StartingScreen from "./components/StartingScreen";
import TranslationScreen from "./components/translation/TranslationScreen";
import ProfileScreen from "./components/profile/ProfileScreen";


function App() {
  return (
    <div className="App">
      <Router>
      
        <Route path="/" exact component  = {StartingScreen}/>
        <Route path="/translation" component = {TranslationScreen}/>
        <Route path="/profile" component = {ProfileScreen}/>
         
    </Router>
    </div>
  );
}

export default App;
