import "../../css/translationCard.css";
import imageArray from "../../image_functions/importAll";

function ProfileScreenCard(props){

    //imports all signs and stores them in an array
    const imageSigns=imageArray()

    // word passed from parent split into individual letters
    const letterArray =props.word.split("");

    return(

        <div className="tcard-main-container">

            <div className="tcard-header">
                <p>{props.word}</p>
            </div>
            <div className="tcard-body">
                <div className="translation">
                    {/*maps every letter from word passed from parent*/}
                    {letterArray.map(letter => (
                        (imageSigns[`${letter}.png`] && <img width="100px" src={imageSigns[`${letter}.png`].default} alt="asd"/>)
                    ))}
                </div>
            </div>

        </div>


    )
}

export default ProfileScreenCard