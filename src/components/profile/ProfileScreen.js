import { ReactComponent as HistoryLogo } from './../../svg/translationhistory.svg';
import { ReactComponent as Avatar } from './../../svg/avatar.svg';
import {ReactComponent as LogOutComp} from './../../svg/logout.svg';
import "../../css/translationScreen.css";
import "../../css/profileScreen.css";
import {Link,useHistory} from "react-router-dom";
import ProfileScreenCard from "./ProfileScreenCard";


function ProfileScreen() {

    const history = useHistory();


    // get local storage data
    const getDataFromStorage = () => {
        let storedTranslations = JSON.parse(localStorage.getItem("translations"));
        return storedTranslations
    }

    const getName = () => {
        let data = sessionStorage.getItem("name");
        return data
    }

    const backToHome = () => {
        history.push("/")
    }

    const logOut =() =>{
        // deleteLocalStorage
        localStorage.clear()
        history.push("/")
    }


    return(        
        <div className="main-container-profile">
            <div className="header-trans">
 
            <HistoryLogo onClick={backToHome} className="trans-logo-2"/>
                <div className="avatar-name">
                    <Link to="/profile">
                    <div className="name-prof">{getName()}</div>
                    <Avatar className="avatar" />
                    </Link>
                    <LogOutComp onClick={logOut} className="log-out"></LogOutComp>
                </div>

            </div>
    
            <div className="translation-card-2">
                {getDataFromStorage().map(wordToPass=>(
                <ProfileScreenCard word={wordToPass} className="t-s-card"/>))}
                <div className="padding">quickfix</div>  
                <div className="padding">quickfix</div>  
            </div>
        </div>

    )
}


export default ProfileScreen