import {useHistory} from "react-router-dom"
import { ReactComponent as TranslationLogo } from './../../svg/translationlogo.svg';
import { ReactComponent as Avatar } from './../../svg/avatar.svg';
import TranslationCard from "./TranslationScreenCard"
import { useEffect, useState } from "react";
import "../../css/translationScreen.css";
import imageArray from "../../image_functions/importAll";
function TranslationScreen() {

    const history = useHistory();

    //userinput 
    const[translate, setTranslate] = useState("");

    //word to be translated
    let[wordToTranslate, setWordToTranslate] = useState("");

    //Array of translated words to pass to profilescreen
    const [translatedWords, setTranslatedWords] = useState([]);
        
    //retrieves name from session storage. 
    const getData = () => {
            let data = sessionStorage.getItem("name"); 
            return data
        }
     
    //takes the input from the user and and sets it to translate variable. 
    const onTranslate = event => {
        setTranslate(event.target.value)
    }
    

    //When enter is pressed the word to translate is passed to TranlationCard component to translate from english to sign-language. 
    //Also the word is pushed to the array for saving 10 translation displayed in the ProfileScreen Component. 
    const handleKeyDown = (event) => {
        if(event.key === 'Enter') {
            
            setWordToTranslate(wordToTranslate = translate.toLowerCase())
            setTranslatedWords(translatedWords.concat(translate.toLowerCase()))
            if (translatedWords.length>10){
                let deleteElement=translatedWords
                deleteElement.shift()
                setTranslatedWords(deleteElement)
            }
            localStorage.setItem("translations", JSON.stringify(translatedWords));
            
            setTranslate(event.target.value = "")
            
        }

        
    }

    
    const images=imageArray()
    
    const backToHome = () => {
        history.push("/")
    }

    const toProfile = () => {
        history.push("/profile")
        localStorage.setItem("translations", JSON.stringify(translatedWords));
    }
    
    
    return(
        <div className="main-container-trans">
            <div className="header-trans">
                <TranslationLogo onClick={backToHome} className="trans-logo-2"/>
                <div className="avatar-name-trans">
                    <div onClick={toProfile} className="name"> {getData()}</div>
                    <Avatar onClick={toProfile} className="avatar" />
                </div>
                <input type="text" onChange={onTranslate} onKeyPress={handleKeyDown} value={translate} className="enter-translation" placeholder="ENTER WORD TO TRANSLATE"/>
            </div>
            <div className="translation-card">
             <TranslationCard wordToTranslate={wordToTranslate} className="t-s-card"/>
            </div>
        </div>
    )
}
export default TranslationScreen