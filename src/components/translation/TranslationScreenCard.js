import "../../css/translationCard.css";
import imageArray from "../../image_functions/importAll";

function TranslationCard(props){

    //array of sign language icons
    const images=imageArray()
    const letterArray = props.wordToTranslate.split(""); 
 

    return(
    <div className="tcard-main-container">
        <div className="tcard-header">
            <p>TRANSLATION</p>
        </div>
       
        <div className="tcard-body">
            <div className="translation">
            {/* for each letter in the word to translate it is matched with the matching image */}
            {letterArray.map(letter => (
                (images[`${letter}.png`] && <img width="100px" src={images[`${letter}.png`].default} alt="asd"/>)
            ))}
            </div>
        </div>
    </div>
    )
}

export default TranslationCard