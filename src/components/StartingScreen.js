
import "../../src/css/startingScreen.css";
import { ReactComponent as MainLogo } from '../svg/mainlogo.svg';
import { ReactComponent as LogoTrans } from '../svg/logotrans.svg';
import { useState } from "react";
import { useHistory } from "react-router-dom";

function StartingScreen() {

    const history = useHistory(); 

    //hook for setting name to pass to translationscreen
    const[name, setName] = useState("");
    
    //when name is input the setName function is called setting the name value to input by user
    const onChangeName = event => {
        setName(event.target.value)
    }

    //stores the value entered on enter key pressed to session storage and redirects to /translation
    const handleKeyDown = (event) => {
        if(event.key === 'Enter') {
            sessionStorage.setItem("name", name);
            history.push("/translation")
            
        }  
    }
    return(
        <div className="main-container">
            <div className="header">
                <div className="main-logo">
                    <MainLogo/> 
                </div>
            </div>
            <div className="mid-screen">
                <div className="trans-logo">
                    <LogoTrans className="trans-logo2"/> 
                </div>
            </div>
            <div className="bottom-screen">
                <input type="text" onChange={onChangeName} onKeyPress={handleKeyDown} value={name} className="enter-name" placeholder="ENTER YOUR NAME"/>    
            </div>
        </div>
    )

}

export default StartingScreen