

function imageArray(){
    function importAll(r) {
        let images = {};
        r.keys().map((item, index) => { images[item.replace('./', '')] = r(item); });
        return images;
    }

    const images = importAll(require.context( './../individial_signs' ,  false , /\.png/));
    return images
}

export default imageArray